[toc]

# 项目简介

用于连接数据库，对指定的数据表创建其实体类。



# 使用说明

在 application.yml 文件中配置好自己的数据库。

随后启动 GenerateEntityFromTableApplication 类。

控制台会输出：

```
	Application 'GenerateEntity' is running! Access URLs:
	Local: 		http://localhost:80
	External: 	http://192.168.1.2:80
	Doc: 	http://192.168.1.2:80/doc.html
```

访问最后一行 Doc 对应的连接地址。会进入 swagger 的测试界面。

![](img\20220724131803.png)

点击进行调试：

![20220724131942](img\20220724131942.png)

输入参数后，点击发送按钮，在响应结果中会带出你想要的结果，比如：

```java
package org.feng.entity;

import lombok.Data;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * 宝宝写的一实体类
 *
 * @version v2.0
 * @author: 宝宝
 * @date: 2022年07月24日 13时19分
 */
@Data
@Table("student")
public class StudentData {

    @Column("id")
    private Integer id;
    @Column("name")
    private String name;
    @Column("age")
    private Integer age;
    @Column("weight")
    private Double weight;
    @Column("height")
    private Double height;
    @Column("modified")
    private LocalDateTime modified;
}
```

目前的缺点是，没有对`LocalDateTime` 等特殊类型做兼容，需要手动导入包。

以上的实体类，是从数据库表中查 student 表，生成的。
