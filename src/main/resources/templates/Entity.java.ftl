package ${packageName};

<#if lombokData && lombok>
import lombok.Data;
<#elseif lombok>
import lombok.Getter;
</#if>
<#if lombokToString>
import lombok.ToString;
</#if>
<#if gson>
import com.google.gson.annotations.SerializedName;
</#if>
<#if jpa>
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;
</#if>

/**
 * ${classDoc}
 *
 * @version ${version}
 * @author: ${author}
 * @date: ${date}
 */
<#if lombokData && lombok>
@Data
<#elseif lombok>
@Getter
</#if>
<#if lombokToString>
@ToString
</#if>
<#if jpa>
@Table("${tableName}")
</#if>
public class ${className} {

<#list fields as field>
    <#if jpa>
    @Column("${field.name}")
    </#if>
    <#if gson>
    @SerializedName("${field.name}")
    </#if>
    private ${field.simpleTypeName} ${field.fieldName};
</#list>
}
