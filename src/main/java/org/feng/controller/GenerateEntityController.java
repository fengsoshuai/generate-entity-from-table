package org.feng.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.feng.entity.FreeMarkerEntity;
import org.feng.entity.GenerateEntityDTO;
import org.feng.entity.TableMetaData;
import org.feng.parser.MysqlDmlParser;
import org.feng.parser.TemplateProcessor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 生成实体类控制器
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年07月23日 11时56分
 */
@Slf4j
@Api(tags = "生成实体类控制器")
@RestController
public class GenerateEntityController {

    @Resource
    private MysqlDmlParser mysqlDmlParser;

    @ApiOperation(value = "生成实体类接口")
    @GetMapping("/generate")
    public String generateEntity(GenerateEntityDTO generateEntityDto) {
        // 获取表名
        String tableName = generateEntityDto.getTableName();
        // 查表获取元数据
        TableMetaData tableMetaData = mysqlDmlParser.parser(tableName);

        FreeMarkerEntity freeMarkerEntity = new FreeMarkerEntity();
        freeMarkerEntity.setAuthor(generateEntityDto.getAuthor());
        freeMarkerEntity.setClassDoc(generateEntityDto.getClassDoc());
        freeMarkerEntity.setLombok(generateEntityDto.getLombok());
        freeMarkerEntity.setTableMetaData(tableMetaData);
        freeMarkerEntity.setVersion(generateEntityDto.getVersion());
        freeMarkerEntity.setGson(generateEntityDto.getGson());
        freeMarkerEntity.setJpa(generateEntityDto.getJpa());
        freeMarkerEntity.setClassName(generateEntityDto.getClassName());
        freeMarkerEntity.setPackageName(generateEntityDto.getPackageName());
        // 渲染模板
        TemplateProcessor templateProcessor = new TemplateProcessor(freeMarkerEntity);
        String process = templateProcessor.process();
        log.info("实体类 ：\n{}", process);
        return process;
    }
}
