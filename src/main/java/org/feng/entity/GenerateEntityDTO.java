package org.feng.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 生成实体请求对象
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年07月23日 19时38分
 */
@Data
@ApiModel
public class GenerateEntityDTO {
    @ApiModelProperty(value = "表名", required = true)
    private String tableName;
    @ApiModelProperty(value = "类注释中的作者", required = true)
    private String author;
    @ApiModelProperty(value = "类注释中的版本", required = true)
    private String version;
    @ApiModelProperty(value = "类文档注释", required = true)
    private String classDoc;
    @ApiModelProperty(value = "生成的类所在的包名", required = true)
    private String packageName;
    /**
     * 自定义类名
     */
    @ApiModelProperty(value = "类名，不传类名时，使用表名生成")
    private String className;
    /**
     * 租户key，预留给多数据源场景
     */
    @ApiModelProperty(value = "租户key，多数据源备用，目前尚未支持")
    private String merchantKey;
    @ApiModelProperty(value = "是否使用lombok注解，默认使用 @Data")
    private Boolean lombok;
    @ApiModelProperty(value = "是否使用Gson的注解，默认不使用 @SerializedName")
    private Boolean gson;
    @ApiModelProperty(value = "是否使用jpa注解，默认不使用 @Column、@Table")
    private Boolean jpa;

    public GenerateEntityDTO() {
        this.lombok = true;
        this.gson = false;
        this.jpa = false;
    }
}
