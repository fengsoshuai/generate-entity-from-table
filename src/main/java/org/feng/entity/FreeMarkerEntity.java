package org.feng.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Freemarker 的实体，用于渲染模板的类
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年07月23日 10时47分
 */
@Getter
@Setter
public class FreeMarkerEntity {
    private static String dateFormat = "yyyy年MM月dd日 HH时mm分";
    public static String date = LocalDateTime.now().atZone(ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(dateFormat));

    @Setter(AccessLevel.NONE)
    private boolean lombok;
    private boolean gson;
    private boolean jpa;

    private boolean lombokData;
    private boolean lombokToString;

    private String author;
    private String version;
    private String classDoc;
    private String className;
    private String packageName;

    private TableMetaData tableMetaData;

    public void setLombok(boolean lombok) {
        this.lombok = lombok;
        this.lombokData = lombok;
    }
}
