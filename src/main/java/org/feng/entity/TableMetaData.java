package org.feng.entity;

import lombok.Data;
import lombok.Getter;
import lombok.ToString;
import org.feng.util.StringUtil;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 数据库表的元数据信息
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年07月23日 10时12分
 */
@ToString
@Data
public class TableMetaData {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 列信息
     */
    private List<ColumnInfo> columnInfos;

    /**
     * 将数据库表名转换为目标类名，一般表名可能带有下划线
     *
     * @return 目标实体类名
     */
    public String toClassName() {
        return StringUtil.parseToClassName(tableName);
    }

    /**
     * 列信息
     */
    @ToString
    @Getter
    public static class ColumnInfo {
        /**
         * 列名
         */
        private String name;
        /**
         * 变量名：通过列名解析获取，比如去除下划线，首字母小写
         */
        private String fieldName;
        /**
         * 列对应的java类型，比如 java.lang.Integer
         */
        private String fullTypeName;
        /**
         * 列对应的java类型，不带包名，比如 Integer
         */
        private String simpleTypeName;

        public void setFullTypeName(String fullTypeName) {
            fullTypeName = TypeConvert.convert(fullTypeName);
            this.fullTypeName = fullTypeName;
            this.simpleTypeName = generateSimpleTypeName(fullTypeName);
        }

        private String generateSimpleTypeName(String fullTypeName) {
            int lastIndexOf = fullTypeName.lastIndexOf(".");
            return fullTypeName.substring(lastIndexOf + 1);
        }

        public void setName(String name) {
            this.name = name;
            this.fieldName = toFieldName(name);
        }

        private String toFieldName(String name) {
            return StringUtil.parseToFieldName(name);
        }
    }

    /**
     * 类型转换：自定义需要的类型，从从数据库查到的类型，不一定是你想用的。
     *
     * @version v1.0
     * @author: fengjinsong
     * @date: 2022年07月23日 10时26分
     */
    public static class TypeConvert {
        private final static String TIMESTAMP = "java.sql.Timestamp";
        public static String convert(String fullTypeName) {
            if (TIMESTAMP.equals(fullTypeName)) {
                return LocalDateTime.class.getName();
            }

            // 其他类型转换
            return fullTypeName;
        }
    }
}
