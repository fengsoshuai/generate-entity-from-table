package org.feng.parser;

import org.feng.entity.TableMetaData;

/**
 * MySql 元数据解析接口
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年07月23日 09时38分
 */
public interface MysqlDmlParser {

    /**
     * 查表获取元数据信息
     *
     * @param tableName 表名
     * @return 表元数据
     */
    TableMetaData parser(String tableName);
}
