package org.feng.util;

import freemarker.template.Configuration;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * FreeMarker 工具类
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年07月23日 10时57分
 */
@Slf4j
public class FreeMarkerUtil {
    public static Configuration getConfiguration() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_22);
        URL resource = FreeMarkerUtil.class.getClassLoader().getResource("org/feng/util");
        assert resource != null;
        String path = resource.getPath();
        int indexOf = path.indexOf("/org/feng/util");
        String charSequence = path.substring(1, indexOf);
        String projectPath = charSequence.replace("/", "\\");
        String directory = projectPath + "\\templates";
        log.info("模板所在路径 {}", directory);
        try {
            configuration.setDirectoryForTemplateLoading(new File(directory));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return configuration;
    }
}
