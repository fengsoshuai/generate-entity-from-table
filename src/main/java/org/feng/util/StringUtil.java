package org.feng.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @version v1.0
 * @author: fengjinsong
 * @date: 2022年07月23日 10时35分
 */
public class StringUtil {
    public static final Pattern JSON_KEY_PATTERN = Pattern.compile("[A-Za-z0-9]+");

    public static String parseToFieldName(String name) {
        StringBuilder fieldName = new StringBuilder();
        Matcher matcher = JSON_KEY_PATTERN.matcher(name);

        boolean isFirstWord = true;
        while (matcher.find()) {
            String word = matcher.group();
            if (isFirstWord) {
                fieldName.append(lowerFirstChar(word));
                isFirstWord = false;
                continue;
            }
            // 后边的单词
            fieldName.append(upperFirstChar(word));
        }
        return fieldName.toString();
    }

    public static String parseToClassName(String name) {
        StringBuilder fieldName = new StringBuilder();
        Matcher matcher = JSON_KEY_PATTERN.matcher(name);

        boolean isFirstWord = true;
        while (matcher.find()) {
            String word = matcher.group();
            if (isFirstWord) {
                fieldName.append(upperFirstChar(word));
                isFirstWord = false;
                continue;
            }
            // 后边的单词
            fieldName.append(upperFirstChar(word));
        }
        return fieldName.toString();
    }

    public static String lowerFirstChar(String word) {
        char firstChar = Character.toLowerCase(word.charAt(0));
        return firstChar + word.substring(1);
    }

    public static String upperFirstChar(String word) {
        char firstChar = Character.toUpperCase(word.charAt(0));
        return firstChar + word.substring(1);
    }
}
